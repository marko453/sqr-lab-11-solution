# Lab 11 - Security Check

Student name: <b>Marko Pezer</b><br>
Student email: <b>m.pezer@innopolis.university</b><br>
Student group: <b>BS18-SE-01</b>

## Solution

Solution for Lab 11 can be found here: [Google Docs](https://docs.google.com/document/d/1UybCJXCdLdpxlvjjjmuXDE9ofZxYepXFYkQidhs9AfI/edit?usp=sharing)
